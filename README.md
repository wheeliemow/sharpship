#SharpShip
=========

C# .Net Wrapper for UPS shipping API 


## License 
SharpShip is licenced under the Apache license and is free to use and modify how ever you would like. 

## Nuget 
[Nuget package found here.](https://www.nuget.org/packages/SharpShip/1.0.0) 

##Functions
* Street Level Address Validation
* Address Validation
* Rate Checking

## Use
To use the library you will need to sign up for an account with [UPS](https://www.ups.com/upsdeveloperkit/downloadresource?loc=en_US)

First, request an access key from UPS. This is used to validate your request with UPS, along with your account credentials. 

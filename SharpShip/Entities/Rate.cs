﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpShip.Entities
{
    public class Rate
    {

        public string Provider { get; set; }

        public string ProviderCode { get; set; }

        public decimal TotalCharges { get; set; }

        public string ServiceDescription { get; set; }



    }
}

﻿using System.Collections.Generic;
using System.Linq;

namespace SharpShip.Entities
{
    public class Shippment
    {
        public Address _shipper { get; set; }

        public Address _destination { get; set; }

        public List<Package> _packages { get; set; }

        public int PackageCount { get { return _packages.Count(); } }

        public decimal TotalShipmentWeight { get { return _packages.Sum(m => m.RoundWeight); } }

        public Shippment(Address shipper, Address destination, List<Package> packages)
        {
            _shipper = shipper;
            _destination = destination;
            _packages = packages;
        }
    }
}
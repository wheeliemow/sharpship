﻿using System;
using System.Collections.Generic;

namespace SharpShip.Entities
{
    public class Address : IComparable
    {
        public string BuildingName { get; set; }

        public string ReceiverName { get; set; }

        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public string AddressLine3 { get; set; }

        public string City { get; set; }

        public string StateProvince { get; set; }

        public string PostalCode { get; set; }

        public string CountryCode { get; set; }

        public bool IsResidential { get; set; }

        public bool IsUSAddress()
        {
            return (!string.IsNullOrWhiteSpace(CountryCode) && string.Equals(CountryCode, "US", System.StringComparison.OrdinalIgnoreCase));
        }

        public int CompareTo(object obj)
        {
            if (obj == null) return -1;

            Address otherAddress = obj as Address;

            if (this.City.ToLower().Trim().Equals(otherAddress.City.ToLower().Trim()) && this.StateProvince.ToLower().Trim().Equals(otherAddress.StateProvince.ToLower().Trim()) && this.PostalCode.ToLower().Trim().Equals(otherAddress.PostalCode.ToLower().Trim()))
                return 0;

            return 1;
        }

        public bool Contains(List<Address> list)
        {
            foreach (var item in list)
            {
                if (this.CompareTo(item) == 0)
                    return true;
            }

            return false;
        }
    }
}
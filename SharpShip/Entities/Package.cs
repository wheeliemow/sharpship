﻿using System;

namespace SharpShip.Entities
{
    public class Package
    {
        public decimal Length { get; set; }

        public decimal Height { get; set; }

        public decimal Width { get; set; }

        public decimal InsuranceValue { get; set; }

        public decimal Weight { get; set; }

        public bool IsOversized { get; set; }

        public string Packaging { get; set; }

        public decimal RoundHeight
        {
            get { return Math.Ceiling(Height); }
        }

        public decimal RoundWidth
        {
            get { return Math.Ceiling(Width); }
        }

        public decimal RoundLength
        {
            get { return Math.Ceiling(Length); }
        }

        public decimal RoundWeight
        {
            get { return Math.Ceiling(Weight); }
        }
    }
}
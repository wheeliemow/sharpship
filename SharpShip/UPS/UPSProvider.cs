﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using SharpShip.Abstract;
using SharpShip.Entities;

namespace SharpShip.UPS
{
    public class UPSProvider : IShippingProvider
    {
        #region Properties

        private const string rateUrl = "https://www.ups.com/ups.app/xml/Rate";

        public string _licenseNumber { get; set; }

        public string _userID { get; set; }

        public string _password { get; set; }

        public int _defaultTimeout { get; set; }

        public string _serviceDescription { get; set; }

        public Dictionary<string, string> services { get; set; }

        #endregion Properties

        public UPSProvider(string license, string username, string password, int defaultTimeout = 10, string serviceDescription = null)
        {
            _licenseNumber = license;
            _userID = username;
            _password = password;
            _defaultTimeout = defaultTimeout;
            _serviceDescription = serviceDescription;
        }

        public List<Rate> GetRates(Shippment shippment)
        {
            var request = (HttpWebRequest)WebRequest.Create(rateUrl);
            request.Method = "POST";
            byte[] bytes = buildRequestMessage(shippment);

            request.ContentLength = bytes.Length;
            Stream stream = request.GetRequestStream();
            stream.Write(bytes, 0, bytes.Length);
            stream.Close();
            var response = (HttpWebResponse)request.GetResponse();

            List<Rate> returnRates = new List<Rate>();
            var result = parseResponseMessage(new StreamReader(response.GetResponseStream()).ReadToEnd(), ref returnRates);
            response.Close();

            return returnRates;
        }

        private bool parseResponseMessage(string response, ref List<Rate> rateList)
        {
            XDocument xdoc = XDocument.Parse(response);

            string success = xdoc.Descendants().FirstOrDefault(m => m.Name == "ResponseStatusDescription").Value;

            if (!success.Equals("Success", StringComparison.OrdinalIgnoreCase))
            {
                return false;
            }

            loadservices();
          
            var result = xdoc.Descendants("RatedShipment").Select(r => new Rate
            {
                TotalCharges = decimal.Parse(r.Element("TotalCharges").Element("MonetaryValue").Value),
                ServiceDescription = services.ContainsKey (r.Element("Service").Element("Code").Value.ToString()) ? services.FirstOrDefault(sr => sr.Key ==  r.Element("Service").Element("Code").Value).Value : ""
            });

            rateList = result.ToList<Rate>();

            return true;
        }

        private byte[] buildRequestMessage(Shippment shippment)
        {
            // StringWriter str = new StringWriter();
            var writer = new XmlTextWriter(new MemoryStream(2000), new UTF8Encoding(false));

            XDocument xdoc = new XDocument(new XDeclaration("1.0", "utf-16", "true"),
                new XElement("AccessRequest", new XAttribute("lang", "en-US"),
                new XElement("AccessLicenseNumber", _licenseNumber),
                new XElement("UserId", _userID),
                new XElement("Password", _password)));

            xdoc.Save(writer);

            List<XElement> sb = new List<XElement>();

            XElement shipper = new XElement("Shipper",
                new XElement("Address",
                                    new XElement("AddressLine1", shippment._shipper.AddressLine1),
                                    new XElement("City", shippment._shipper.City),
                                    new XElement("StateProvinceCode", shippment._shipper.StateProvince),
                                    new XElement("PostalCode", shippment._shipper.PostalCode),
                                    new XElement("CountryCode", shippment._shipper.CountryCode)

                                    )

                );
            sb.Add(shipper);

            XElement shipto = new XElement("ShipTo",
                           new XElement("Address",
                                        new XElement("AddressLine1", shippment._destination.AddressLine1),
                                        new XElement("AddressLine2", shippment._destination.AddressLine2),
                                        new XElement("AddressLine3", shippment._destination.AddressLine3),
                                        new XElement("City", shippment._destination.City),
                                        new XElement("StateProvinceCode", shippment._destination.StateProvince),
                                        new XElement("PostalCode", shippment._destination.PostalCode),
                                        new XElement("CountryCode", shippment._destination.CountryCode)

                                        )

                );
            sb.Add(shipto);

            XElement servicecode = new XElement("Service", new XElement("Code", "03"));

            sb.Add(servicecode);

            foreach (Package pack in shippment._packages)
            {
                XElement xTree = new XElement("Package",
                    new XElement("PackagingType",
                        new XElement("Code", "02")),
                    new XElement("Description", "Rate"),
                    new XElement("PackageWeight",
                        new XElement("UnitOfMeasure",
                            new XElement("Code", "LBS")),
                        new XElement("Weight", pack.RoundWeight.ToString())
                            )

                    );
                sb.Add(xTree);
            }

            XDocument xDoc2 = new XDocument(
                 new XDeclaration("1.0", "utf-16", "true"),
                  new XElement("RatingServiceSelectionRequest", new XAttribute("lang", "en-US"),
                      new XElement("Request",
                          new XElement("TransactionReference",
                              new XElement("CustomerContext", "Rating and Service"),
                               new XElement("XpciVersion", "1.0001")
                            ),
                            new XElement("RequestAction", "Rate"),
                            new XElement("RequestOption", "Shop")

                        ),

                        new XElement("Shipment", sb

                                )

                        )

            );

            xDoc2.Save(writer);

            writer.Flush();
            var buffer = new byte[writer.BaseStream.Length];
            writer.BaseStream.Position = 0;
            writer.BaseStream.Read(buffer, 0, buffer.Length);
            writer.Close();

            return buffer;
        }

        private void loadservices()
        {
            services = new Dictionary<string, string>();
            services.Add("03", "Ground");
            services.Add("02", "2nd Day Air");
            services.Add("01", "Next Day Air");
            services.Add("14", "Next Day Air Early");
            services.Add("13", "Next Day Air Saver");
            services.Add("59", "2nd Day Air AM");
            services.Add("12", "3 Day Ground Select");
            services.Add("11", "International Standard");
            services.Add("07", "Worldwide Express");
            services.Add("54", "Worldwide Express Plus");
            services.Add("08", "Worldwide Expedited");
            services.Add("65", "International Saver");
        }
    }
}
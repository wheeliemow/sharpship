﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using SharpShip.Entities;

namespace SharpShip.UPS
{
    public class StreetLevelAddressValidator
    {
       
        private const string validationUrl = "https://onlinetools.ups.com/ups.app/xml/XAV";

        private readonly string _licenseNumber;
        private readonly string _password;
        private readonly string _userID;

        private string _listSize;

        public StreetLevelAddressValidator(string licenseNumber, string password, string userID)
        {
            _licenseNumber = licenseNumber;
            _userID = userID;
            _password = password;
            
        }

        /// <summary>
        /// Validates a street address using UPS API. listSize is the number of addresses that should be returned. 
        /// </summary>
        /// <param name="address"></param>
        /// <returns>returns a list of possible matches for the address if the address has no matches</returns>
        public bool Validate(Address address, ref List<Address> returnAddressList, string listSize = "3")
        {
            _listSize = listSize;
            var request = (HttpWebRequest)WebRequest.Create(validationUrl);
            
            request.Method = "POST";
            request.ContentType = "text/xml; encoding=UTF-8";

            var bytes = buildRequestMessage(address);
            request.ContentLength = bytes.Length;

            Stream stream = request.GetRequestStream();
            stream.Write(bytes, 0, bytes.Length);
            stream.Close();

            var response = (HttpWebResponse)request.GetResponse();

            var result = parseResponseMessage(new StreamReader(response.GetResponseStream()).ReadToEnd(), ref returnAddressList);
            response.Close();

            return address.Contains(returnAddressList) || returnAddressList.Count == 0;
        }

        private bool parseResponseMessage(string response, ref List<Address> addressList)
        {
            XDocument xdoc = XDocument.Parse(response);

            string success = xdoc.Descendants().FirstOrDefault(m => m.Name == "ResponseStatusDescription").Value;

            if (!success.Equals("Success", StringComparison.OrdinalIgnoreCase))
            {
                addressList.Clear();
                return false;
            }

            var result = xdoc.Descendants("AddressKeyFormat").Select(a => new Address
                {
                    AddressLine1 = a.Element("AddressLine").Value,
                    City = a.Element("PoliticalDivision2").Value,
                    StateProvince = a.Element("PoliticalDivision1").Value,
                    PostalCode = a.Element("PostcodePrimaryLow").Value,
                    CountryCode = a.Element("CountryCode").Value
                });

            addressList = result.ToList<Address>();

            return true;
        }

        private byte[] buildRequestMessage(Address address)
        {
            // StringWriter str = new StringWriter();
            var writer = new XmlTextWriter(new MemoryStream(2000), new UTF8Encoding(false));

            XDocument xdoc = new XDocument(new XDeclaration("1.0", "utf-16", "true"),
                new XElement("AccessRequest", new XAttribute("lang", "en-US"),
                new XElement("AccessLicenseNumber", _licenseNumber),
                new XElement("UserId", _userID),
                new XElement("Password", _password)));

            xdoc.Save(writer);

            XDocument xDoc2 = new XDocument(
                 new XDeclaration("1.0", "utf-16", "true"),
                  new XElement("AddressValidationRequest", new XAttribute("lang", "en-US"),
                      new XElement("Request",
                          new XElement("TransactionReference",
                              new XElement("CustomerContext"),
                               new XElement("XpciVersion", "1.0001")
                            ),
                            new XElement("RequestAction", "XAV"),
                            new XElement("RequestOption", "3")

                        ),
                        new XElement("MaximumListSize", _listSize),
                        new XElement("AddressKeyFormat",
                            new XElement("ConsigneeName", address.ReceiverName),
                            new XElement("BuildingName", address.BuildingName),
                            new XElement("AddressLine", address.AddressLine1),
                            new XElement("AddressLine", address.AddressLine2),
                            new XElement("AddressLine", address.AddressLine3),
                            new XElement("PoliticalDivision2", address.City),
                            new XElement("PoliticalDivision1", address.StateProvince),
                            new XElement("PostcodePrimaryLow", address.PostalCode),
                            new XElement("CountryCode", address.CountryCode)
                            )

                    )

            );

            xDoc2.Save(writer);

            writer.Flush();
            var buffer = new byte[writer.BaseStream.Length];
            writer.BaseStream.Position = 0;
            writer.BaseStream.Read(buffer, 0, buffer.Length);
            writer.Close();

            return buffer;
        }
    }
}
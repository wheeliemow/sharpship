﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using SharpShip.Entities;

namespace SharpShip.UPS
{
    public class AddressValidator
    {
      
        private const string validationUrl = "https://onlinetools.ups.com/ups.app/xml/AV";

        private readonly string _licenseNumber;
        private readonly string _password;
        private readonly string _userID;

        public AddressValidator(string licenseNumber, string password, string userName)
        {
            _licenseNumber = licenseNumber;
            _password = password;
            _userID = userName;
           
        }

        public bool Validate(Address address, ref List<Address> returnAddressList)
        {
            var request = (HttpWebRequest)WebRequest.Create(validationUrl);
            request.Method = "POST";
            request.ContentType = "text/xml; encoding=UTF-8";
            byte[] bytes = buildRequestMessage(address);

            request.ContentLength = bytes.Length;
            Stream stream = request.GetRequestStream();
            stream.Write(bytes, 0, bytes.Length);
            stream.Close();
            var response = (HttpWebResponse)request.GetResponse();

            var result = parseResponseMessage(new StreamReader(response.GetResponseStream()).ReadToEnd(), ref returnAddressList);
            response.Close();

            return address.Contains(returnAddressList) || returnAddressList.Count == 0;
        }

        private bool parseResponseMessage(string response, ref List<Address> addressList)
        {
            XDocument xdoc = XDocument.Parse(response);

            string success = xdoc.Descendants().FirstOrDefault(m => m.Name == "ResponseStatusDescription").Value;

            if (!success.Equals("Success", StringComparison.OrdinalIgnoreCase))
            {
                addressList.Clear();
                return false;
            }

         
            var result = xdoc.Descendants("AddressValidationResult").Select(a => new Address
            {
                City = a.Element("Address").Element("City").Value,
                StateProvince = a.Element("Address").Element("StateProvinceCode").Value,
                PostalCode = a.Element("PostalCodeLowEnd").Value
             
               
            });

            addressList = result.ToList<Address>();

            return true;
        }

        private byte[] buildRequestMessage(Address address)
        {
            // StringWriter str = new StringWriter();
            var writer = new XmlTextWriter(new MemoryStream(2000), new UTF8Encoding(false));

            XDocument xdoc = new XDocument(new XDeclaration("1.0", "utf-16", "true"),
                new XElement("AccessRequest", new XAttribute("lang", "en-US"),
                new XElement("AccessLicenseNumber", _licenseNumber),
                new XElement("UserId", _userID),
                new XElement("Password", _password)));

            xdoc.Save(writer);

            XDocument xDoc2 = new XDocument(
                 new XDeclaration("1.0", "utf-16", "true"),
                  new XElement("AddressValidationRequest", new XAttribute("lang", "en-US"),
                      new XElement("Request",
                          new XElement("TransactionReference",
                              new XElement("CustomerContext"),
                               new XElement("XpciVersion", "1.0001")
                            ),
                            new XElement("RequestAction", "AV")

                        ),
                        new XElement("Address",

                            new XElement("City", address.City),
                            new XElement("StateProvinceCode", address.StateProvince),
                            new XElement("PostalCode", address.PostalCode),
                            new XElement("CountryCode", address.CountryCode)
                            )

                    )

            );

            xDoc2.Save(writer);

            writer.Flush();
            var buffer = new byte[writer.BaseStream.Length];
            writer.BaseStream.Position = 0;
            writer.BaseStream.Read(buffer, 0, buffer.Length);
            writer.Close();

            return buffer;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Configuration;
using SharpShip.Entities;
using SharpShip.UPS;

namespace Sample
{
    internal class Program
    {
        private static readonly string upsLicenseNumber = ConfigurationManager.AppSettings["UPSKey"];
        private static readonly string upsUserId = ConfigurationManager.AppSettings["UPSKey"];
        private static readonly string upsPassword = ConfigurationManager.AppSettings["UPSKey"];

        private static void Main(string[] args)
        {
            //  RunRateChecker();

            // RunStreetLevelValidation();
            RunAddressValidation();

            Console.ReadLine();
        }

        private static void RunRateChecker()
        {
            Address destination = new Address()
            {
                AddressLine1 = "619 Lynn Dr",
                City = "West Salem",
                StateProvince = "WI",
                CountryCode = "US",
                PostalCode = "54669"
            };

            Address shipper = new Address()
            {
                City = "Winona",
                StateProvince = "MN",
                PostalCode = "55987",
                CountryCode = "US"
            };

            List<Package> packages = new List<Package>()
            {
                new Package() {Weight = 12},
                new Package() {Weight = 10}
            };

            Shippment shippment = new Shippment(shipper, destination, packages);

            UPSProvider provider = new UPSProvider(upsLicenseNumber, upsUserId, upsPassword);

            List<Rate> result = provider.GetRates(shippment);

            foreach (var rate in result)
            {
                Console.WriteLine(rate.ServiceDescription);
                Console.WriteLine(rate.TotalCharges);
            }
        }

        private static void RunStreetLevelValidation()
        {
            StreetLevelAddressValidator validator = new StreetLevelAddressValidator(upsLicenseNumber, upsPassword, upsUserId);
            Address myAddress = new Address()
            {
                AddressLine1 = "619 Lynn Dr",
                City = "West Salem",
                StateProvince = "MN",
                CountryCode = "US",
                PostalCode = "55987"
            };

            List<Address> results = new List<Address>();
            var isvalid = validator.Validate(myAddress, ref results);

            Console.WriteLine(isvalid.ToString());

            foreach (var address in results)
            {
                Console.WriteLine(address.AddressLine1);
                Console.WriteLine(address.City);
                Console.WriteLine(address.StateProvince);
                Console.WriteLine(address.PostalCode);
                Console.WriteLine(address.CountryCode);
            }
        }

        private static void RunAddressValidation()
        {
            Address myAddress = new Address()
            {
                City = "West Salem",
                StateProvince = "MN",
                CountryCode = "US",
                PostalCode = "55987"
            };

            AddressValidator aValidator = new AddressValidator(upsLicenseNumber, upsPassword, upsUserId);

            List<Address> returnAddress = new List<Address>();

            bool valid = aValidator.Validate(myAddress, ref returnAddress);
            Console.WriteLine(valid);

            foreach (var address in returnAddress)
            {
                Console.WriteLine(address.City);
                Console.WriteLine(address.StateProvince);
                Console.WriteLine(address.PostalCode);
                Console.WriteLine(address.CountryCode);
            }
        }
    }
}